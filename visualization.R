cwd <- getwd()
#cwd <- 'C:\\Users\\pteja\\OneDrive\\Desktop\\b_group-139\\'

setwd(cwd)

# Attribute Information:
#   1. vendor name: 30 
# (adviser, amdahl,apollo, basf, bti, burroughs, c.r.d, cambex, cdc, dec, 
#   dg, formation, four-phase, gould, honeywell, hp, ibm, ipl, magnuson, 
#   microdata, nas, ncr, nixdorf, perkin-elmer, prime, siemens, sperry, 
#   sratus, wang)
# 2. Model Name: many unique symbols
# 3. MYCT: machine cycle time in nanoseconds (integer)
# 4. MMIN: minimum main memory in kilobytes (integer)
# 5. MMAX: maximum main memory in kilobytes (integer)
# 6. CACH: cache memory in kilobytes (integer)
# 7. CHMIN: minimum channels in units (integer)
# 8. CHMAX: maximum channels in units (integer)
# 9. PRP: published relative performance (integer)
# 10. ERP: estimated relative performance from the original article (integer)


# list of column names
cols = c("vendor_name", "Model_Name", "MYCT", "MMIN", "MMAX", "CACH", "CHMIN", "CHMAX", "PRP","ERP")

# loading csv file
comHard <- read.csv(file = 'machine.data.csv', col.names =cols, header=FALSE)

# checking top 5 rows
head(comHard)

# checking column names
colnames(comHard)


# splitting dependent variable(target variable)
y <- comHard$PRP

# created lists with column names and codes for generating scatter plots
col_codes <- c("MYCT", "MMIN", "MMAX", "CACH", "CHMIN", "CHMAX")
col_abbv <- c("machine cycle time in nanoseconds",
              "minimum main memory in kilobytes",
              "maximum main memory in kilobytes",
              "cache memory in kilobytes",
              "minimum channels in units",
              "maximum channels in units")



#-----------------------scatter plot---------------------
pdf("visualization.pdf") #  Give the chart file a name.

options(scipen=999)

# looping column codes and abbreviation names
for (item in Map(c,col_codes, col_abbv)){
  
  # scatter plot between two continuous value variables
  plot(comHard[[item[c(1)]]], y, 
       main = paste({item[c(1)]}, 'VS PRP'),
       xlab = paste({item[c(2)]}, '(', {item[c(1)]}, ')'), 
       ylab = "published relative performance (PRP)",
       pch = 20, frame = FALSE)
  
  # plotting regression line between observations
  abline(lm(y ~ comHard[[item[c(1)]]], data = comHard), col = "blue")
  
}


#-----------------------box plot-----------------------
par(mfrow=c(2,3))
for (i in 1:length(col_codes)+1) {
  boxplot(comHard[,i+2], main=names(comHard[i+2]), type="l")
  
}


#---------------------- histogram--------------------------
par(mfrow=c(1,1))
h <- hist(y, breaks = 10, density = 10,
          col = "lightgray", xlab = "published relative performance", main = "Histogram of PRP") 

xfit <- seq(min(y), max(y), length = 40) 
yfit <- dnorm(xfit, mean = mean(y), sd = sd(y)) 
yfit <- yfit * diff(h$mids[1:2]) * length(y) 

lines(xfit, yfit, col = "black", lwd = 2)



# saving pdf file
dev.off() 








