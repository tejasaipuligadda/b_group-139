
# taking current working directory
cwd <- getwd()


# setting current working directory 
setwd(cwd)


# Attribute Information:
#   1. vendor name: 30 
# (adviser, amdahl,apollo, basf, bti, burroughs, c.r.d, cambex, cdc, dec, 
#   dg, formation, four-phase, gould, honeywell, hp, ibm, ipl, magnuson, 
#   microdata, nas, ncr, nixdorf, perkin-elmer, prime, siemens, sperry, 
#   sratus, wang)
# 2. Model Name: many unique symbols
# 3. MYCT: machine cycle time in nanoseconds (integer)
# 4. MMIN: minimum main memory in kilobytes (integer)
# 5. MMAX: maximum main memory in kilobytes (integer)
# 6. CACH: cache memory in kilobytes (integer)
# 7. CHMIN: minimum channels in units (integer)
# 8. CHMAX: maximum channels in units (integer)
# 9. PRP: published relative performance (integer)
# 10. ERP: estimated relative performance from the original article (integer)


cols = c("vendor_name", "Model_Name", "MYCT", "MMIN", "MMAX", "CACH", "CHMIN", "CHMAX", "PRP","ERP")

comHard <- read.csv(file = 'machine.data.csv', col.names =cols, header=FALSE)


x <- comHard$MYCT
y <- comHard$PRP



options(scipen=999)

#---------------- histogram plot with overlayed normal curve.
par(mfrow=c(1,1)) # setting no of plots in one figure

h <- hist(y, breaks = 10, density = 10,
          col = "lightgray", 
          xlab = "published relative performance", 
          main = "Histogram of PRP") 

xfit <- seq(min(y), max(y), length = 40) 
yfit <- dnorm(xfit, mean = mean(y), sd = sd(y)) 
yfit <- yfit * diff(h$mids[1:2]) * length(y) 
lines(xfit, yfit, col = "blue", lwd = 2)

#The plot showed all the values of dependent variable are normally distributed .


cor.test(x, y, method="pearson")
# There is a weak negative correlation between  machine cycle time in nanoseconds and published relative performance.







